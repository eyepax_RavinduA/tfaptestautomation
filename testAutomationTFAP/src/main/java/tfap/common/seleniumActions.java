package tfap.common;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;

public class seleniumActions {
	
	WebDriver _driver;
    Logger _log = Logger.getLogger(seleniumActions.class.getName());
    public seleniumActions(WebDriver driver){
        _driver = driver;
    }

    public WebElement getElement(By by,int timeOutSeconds){
        if(timeOutSeconds>0){
            WebElement webElement = null;
            try {
                Wait<WebDriver> wait = new FluentWait<WebDriver>(_driver)
                        .withTimeout(timeOutSeconds, TimeUnit.SECONDS)
                        .pollingEvery(2, TimeUnit.SECONDS)
                        .ignoring(NoSuchElementException.class);
                                                
                webElement = (WebElement) wait.until(ExpectedConditions.elementToBeClickable(by));
            } catch (NoSuchElementException e) {
                _log.error("Element is not found with given time"+ by.toString());
            }
            return webElement;
        }
        else{
            try{
                return _driver.findElement(by);
            }
            catch (NoSuchElementException e){
                _log.error("Element is not found for given selector");
                return null;
            }
        }
    }

    public WebElement getElement(By by){
       return this.getElement(by,0);
    }

    public String getElementText(By by, int timeouts){
        WebElement _element = getElement(by,timeouts);
        if(_element != null){
            try{
               return _element.getText();
            }catch (Exception e){
                _log.error("Can not fetch the text from element");
            }
        }
        _log.info("Element is null");
        return null;
    }

    public String getElementText(By by){
        return this.getElementText(by,0);
    }
    
    public boolean getVisibilityOfElement(By by,int timeout) {
    	return this.getElement(by, timeout).isDisplayed();
    }
    
    public boolean getVisibilityOfElement(By by) {
    	return this.getElement(by, 0).isDisplayed();
    }

    public void clickElement(By by , int timeouts){
        WebElement _element = getElement(by,timeouts);
        if(_element != null){
            try{
                _element.click();
            }catch (Exception e){
                _log.error("Element is not clickable");
            }
        }
    }
    public void clickElement(By by){
        this.clickElement(by,0);
    }

    
    public void scrollToElement(By by,int timeout) {
    	WebElement _element = this.getElement(by,timeout);
    	((JavascriptExecutor) _driver).executeScript("arguments[0].scrollIntoView(true);", _element);
    }
    
    public void scrollActionToElement(By by,int timeout) {
    	WebElement _element = this.getElement(by,timeout);
    	Actions actions = new Actions(_driver);
    	actions.moveToElement(_element);
    	actions.perform();
    }
    public void setValue(By by,String val,int timeouts){
        WebElement _element = getElement(by,timeouts);
        if(_element != null){
            try{
            	_element.clear();
                _element.sendKeys(val);
            }catch (Exception e){
                _log.error("Can not set values for the element"+by);
            }
        }
    }

    public void setValue(By by, String val){
        this.setValue(by,val,0);
    }

    public String getElementAttributeText(By by,String attribute,int timeOut) {
        WebElement _element = getElement(by,timeOut);
        if (_element != null) {
            try{
                String charSequence = _element.getAttribute(attribute);
                return charSequence;
            }catch (Exception e){
                _log.error("Can not set attribute values for the element");
            }
        }
        _log.info("Element is null");
        return null;
    }

    public String getElementCSSValue(By by,String cssVal,int timeOut) {
        WebElement _element = getElement(by,timeOut);
        if (_element != null) {
            try{
                String charSequence = _element.getCssValue(cssVal);
                return charSequence;
            }catch (Exception ex){
                _log.error("Can not get css values of the element");
            }
        }
        _log.info("Element is null");
        return null;
    }

    public void setDropdownValueByIndex(By by, int index, int timeout){
        WebElement _element = getElement(by,timeout);
        if(_element!= null){
            Select _dropDown = new Select(_element);
            try{
                _dropDown.selectByIndex(index);
            }catch (Exception ex){
                _log.error("Can not set dropdown value by index");
            }
        }

    }

    public void setDropdownValueByVal(By by, String val, int timeout){
        WebElement _element = getElement(by,timeout);
        if(_element!= null){
            Select _dropDown = new Select(_element);
            try{
                _dropDown.selectByValue(val);
            }catch (Exception ex){
                _log.error("Can not set dropdown value by val");
            }
        }
    }

    public void setDropdownValueByText(By by, String text, int timeout){
        WebElement _element = getElement(by,timeout);        
        if(_element!= null){
            Select _dropDown = new Select(_element);
            try{
                _dropDown.selectByVisibleText(text);
            }catch (Exception ex){
                _log.error("Can not set dropdown value by visible text");
            }
        }
    }

    public void triggerUIEvent(String xpath){
        try{
            JavascriptExecutor _js = (JavascriptExecutor) _driver;
            _js.executeScript("triggerUIEvent(){" +
                    "var evt = document.createEvent(\"UIEvents\");" +
                    "var ele = document.evaluate("+xpath+", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;" +
                    "evt.initUIEvent(\"change\", true, true);" +
                    "ele.dispatchEvent(evt);" +
                    "}");
        }catch (Exception ex){
            _log.error("Error when executing trigger UI script");
        }
    }
    
    public void javaScriptClick(String locatorType,String locator) {
    	JavascriptExecutor _js = (JavascriptExecutor) _driver;
    	if(locatorType=="id") {
    		_js.executeScript("document.getElementById('"+locator+"').click();");
    	}else if(locatorType=="xpath") {
    		_log.error("Need to implement xpath JS click");
    	}
    }
    
    public void scrollUpPageJS() {
    	JavascriptExecutor _js = (JavascriptExecutor) _driver;
    	_js.executeScript("window.scrollTo(0,0);");
    	try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
