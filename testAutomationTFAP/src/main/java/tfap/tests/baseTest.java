package tfap.tests;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import tfap.common.seleniumActions;
import tfap.utills.fileReader;
import tfap.utills.listener;
import tfap.utills.takeScreenshots;
import tfap.utills.xray.authentication;
import tfap.utills.xray.createIssue;
import tfap.utills.xray.xrayTestResults;


public class baseTest {
	static WebDriver _driver;
	Logger _log = Logger.getLogger(baseTest.class.getName());
	xrayTestResults _xray = new xrayTestResults();
	createIssue _issue = new createIssue();
	public fileReader _config = new fileReader("tfapConfig.properties");
	
	public WebDriver getDriver(){
	     return _driver;
	    }
	
	@BeforeSuite(alwaysRun=true)
	public void setup() {
		tfap.utills.getDriver gt = new tfap.utills.getDriver();
		try {
			_driver = gt.getLocalWebDriver("chrome");
			_driver.manage().window().maximize();
			System.out.println("Loading the driver");
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		_xray.testExecutionKey(_config.getValue("testExecutionKey"),_config.getValue("testPlanKey"));
	}
	
	@AfterTest
	public void afterTestReport(ITestContext context) {
		
		String testCaseID = context.getAttribute("ID").toString();
		listener _listner = new listener();
		ITestResult _result = _listner.returnTestResult();

 
		String status = null;
        try{
            switch (_result.getStatus()) {

                case ITestResult.SUCCESS:

                    status = "PASSED";

                    break;

                case ITestResult.FAILURE:

                    status = "FAILED";

                    break;

                case ITestResult.SKIP:

                    status = "Skipped";

            }
            
            }catch (Exception e) {
				// TODO: handle exception
			}
        
        if(status=="PASSED") {
			_log.info(context.getAttribute("ID")+" has been passed");
		}
		else {
			_log.error(testCaseID+" has been failed");
			_log.error(context.getAttribute("error"));
			takeScreenshots _snap = new takeScreenshots();
			String fileName = _snap.takeSnap(_driver, testCaseID);
			_log.info(fileName);
			/*String issue_id = _issue.createJiraIssue(_config.getValue("bugProjectKey"), testCaseID+" has been failed", context.getAttribute("error")+"  "+_config.getValue("jira_url")+"/browse/"+testCaseID);
			_log.info("Following issue has been logged - "+issue_id);*/
		}
        
        _xray.tests(testCaseID, status);
		
	}
	
	
	@AfterSuite
	public void tearDown() {
		authentication _auth = new authentication();
		String _token = _auth.bearToken();
		_xray.UpdateTestResults(_token);
		_driver.close();
		
	}

}
