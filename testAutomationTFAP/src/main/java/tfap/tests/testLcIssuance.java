package tfap.tests;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import junit.framework.Assert;
import tfap.common.seleniumActions;
import tfap.pages.applicationListPage;
import tfap.pages.dashboardPage;
import tfap.pages.lcPage;
import tfap.scenarios.fillApplicationDetails;
import tfap.scenarios.pageNavigation;
import tfap.scenarios.userManagementScenarios;
import tfap.utills.fileReader;
import tfap.utills.listener;
import tfap.utills.readExcel;
@Listeners(listener.class)
public class testLcIssuance extends baseTest {

	static WebDriver _driver;
	applicationListPage _appPage;
	lcPage _lcPage;
	readExcel _excel = new readExcel("LCIssuanceTestData");
	Logger _log = Logger.getLogger(testLcIssuance.class.getName());
	static String TFAPID;
	public fileReader _propFile = new fileReader("tfapConfig.properties");
	@BeforeClass
	public void init() {
		_driver = this.getDriver();
	}
	
	@Test(priority=1,groups= {"smoke","login"})
	public void verfiyTheUserLogin(ITestContext context) {
		_log.info("Start test case -- verfiyTheUserLogin");
		context.setAttribute("ID","TFAPT-576");
		_driver.navigate().to(_propFile.getValue("baseUrl"));
		userManagementScenarios _user = new userManagementScenarios(_driver);
		dashboardPage _dbPage = _user.loginToApplication("TFAPSA00", "Password123!");		
		try {
			Assert.assertTrue(_dbPage.getLoggedInUserName().contains("TFAP Admin"));
			_log.info("Logged in assertion has been passed");
		}catch(AssertionError ex) {
			context.setAttribute("error", ex.getMessage());
			_log.error("User login assertion has been failed");
			throw ex;
		}		
	}
	
	@Test(priority=2,groups= {"smoke"},dependsOnMethods= {"verfiyTheUserLogin"})
	public void verifyUserCanNavigateToLCCreate(ITestContext context) {
		_log.info("Start test case -- verifyUserCanNavigateToLCCreate" );
		pageNavigation _nav = new pageNavigation(_driver);
		_appPage = _nav.navigateToApplicationList();
		_lcPage = _nav.navigateToNewLCApplication();
		try {
			Assert.assertTrue(_lcPage.getActiveNewStep().contains("New"));
		}catch (AssertionError ex) {			
			context.setAttribute("error", ex.getMessage());
			_log.error("Active tab verification is failed for new application");
			throw ex;
		}
		
		try {
			Assert.assertTrue(_lcPage.getVisibilityOfApplicationTab());
		}catch (AssertionError ex) {			
			context.setAttribute("error", ex.getMessage());
			_log.error("Failed since application details tab is not been visible");
			throw ex;
		}
		
	}
	
	//@Test(priority=3,groups= {"smoke"},dependsOnMethods= {"verifyUserCanNavigateToLCCreate"})
	public void verifyUserCanSavePrimaryContactPerson(ITestContext context) {
		_log.info("Start test case -- verifyUserCanSavePrimaryContactPerson" );
		fillApplicationDetails _fillApp = new fillApplicationDetails(_driver);		
		_fillApp.initiateNewLC();
		String name = _excel.getCellValue("applicantDetails", 1, 0);
		String number = _excel.getCellValue("applicantDetails", 1, 1);
		String email = _excel.getCellValue("applicantDetails", 1, 2);
		_fillApp.fillPrimaryContactPersonDetails(name, number, email);
		_lcPage.scrollUpPageJS();
		_log.info(_lcPage.getTFApplicationId());
		_lcPage.clickSave();
		try {
			Thread.sleep(3000);
			_lcPage.scrollUpPageJS();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			Assert.assertTrue(_lcPage.getActiveStep().contains("Draft"));
		}catch(AssertionError ex) {
			context.setAttribute("error", ex.getMessage());
			_log.error("Failed since draft tab is not displaying");
			throw ex;
		}
		String ss = _lcPage.getTFApplicationId();
		TFAPID =_lcPage.getTFApplicationId().split(":")[1].split(" ")[1];
		_log.info("Draft application TFAP ID is - "+TFAPID);
		
	}
	
	
	@Test(priority=4,groups= {"smoke"},dependsOnMethods= {"verifyUserCanNavigateToLCCreate"})
	public void verifyUserCanSaveApplicantDetailsWith3rdParty(ITestContext context) {
		_log.info("Start test case -- verifyUserCanSaveApplicantDetailsWith3rdParty" );
		fillApplicationDetails _fillApp = new fillApplicationDetails(_driver);		
		_fillApp.initiateNewLC();
		String name = _excel.getCellValue("applicantDetails", 2, 0);
		String number = _excel.getCellValue("applicantDetails", 2, 1);
		
		_fillApp.fillPrimaryContactPersonDetails(name, number, null);
		
		String companyName = _excel.getCellValue("applicantDetails", 2, 3);
		String country = _excel.getCellValue("applicantDetails", 2, 4);
		String adline1 = _excel.getCellValue("applicantDetails", 2, 5);
		_lcPage.scrollUpPageJS();
		_lcPage.click3rdPartyYes();
		_fillApp.fill3rdPartyInformation(true,companyName,country, adline1, null, null);
		_lcPage.scrollUpPageJS();
		_lcPage.clickSave();
		try {
			Thread.sleep(3000);
			_lcPage.scrollUpPageJS();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			Assert.assertTrue(_lcPage.getActiveStep().contains("Draft"));
		}catch(AssertionError ex) {
			context.setAttribute("error", ex.getMessage());
			_log.error("Failed since draft tab is not displaying");
			throw ex;
		}		
		TFAPID =_lcPage.getTFApplicationId().split(":")[1].split(" ")[1];
		_log.info("Draft application TFAP ID is - "+TFAPID);
		context.setAttribute("tfapid", TFAPID);
	}
	
	@Test(priority=4,groups= {"smoke"},dependsOnMethods= {"verifyUserCanSaveApplicantDetailsWith3rdParty"})
	public void verifyThatUserCanViewDraftApplication(ITestContext context) {
		_log.info("Start test case -- verifyThatUserCanViewDraftApplication" );
		pageNavigation _pnav = new pageNavigation(_driver);
		_appPage =_pnav.navigateToApplicationList();
		_appPage.setTFAPIDForSearch(context.getAttribute("tfapid").toString());
		try {
			Thread.sleep(3000);
			_lcPage.scrollUpPageJS();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
	    	Assert.assertTrue(_appPage.getFirstSearchResultID().contains(context.getAttribute("tfapid").toString()));
	    }catch(AssertionError ex) {
	     context.setAttribute("error", ex.getMessage());
	    _log.error("Search LC record has been failed");	
	    throw ex;
	    }
	    _lcPage = _appPage.clickFirstRecord();
	    try {
	    String appID = _lcPage.getTFApplicationId().split(":")[1].split(" ")[1];
	    Assert.assertTrue(context.getAttribute("tfapid").toString().contains(appID));
	    }catch (AssertionError ex) {
	    context.setAttribute("error", ex.getMessage());
	    _log.error("View search LC application has been failed");
	    throw ex;
		}
	}
	
	@Test(priority=5,groups= {"smoke"},dependsOnMethods= {"verifyThatUserCanViewDraftApplication"})
	public void verifyThatUserCanSetBeneficiaryInformation(ITestContext context) {
		_log.info("Start test case -- verifyThatUserCanSetBeneficiaryInformation" );
		_lcPage.clickBeneficiaryTab();
		try {
		Assert.assertTrue(_lcPage.getBeneficiaryHeadings().contains("Beneficiary Information"));	
		}catch(AssertionError ex) {
		_log.error("Failed to load beneficiary tab");
		context.setAttribute("error", ex.getMessage());
		}
		_lcPage.clickSearchBeneficiary();
		String benName = _excel.getCellValue("beneficiary", 1, 0);
		_lcPage.setBeneficiarySearch(benName);
		try {
			Thread.sleep(2000);			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		_lcPage.clickFirstSearchRecord();
		try {
			Thread.sleep(2000);			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		_lcPage.clickSave();
		
		try {
			Thread.sleep(2000);
			_lcPage.scrollUpPageJS();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			Assert.assertTrue(_lcPage.getBeneficiaryName().contains(benName));
		}catch(AssertionError ex) {
			 context.setAttribute("error", ex.getMessage());
			    _log.error("Failed assertion on verifying beneficiary name");
			throw ex;
		}
	}
	
	@Test(priority=6,groups= {"smoke"},dependsOnMethods= {"verifyThatUserCanSetBeneficiaryInformation"})
	public void verifyThatUserCanSaveLCTerm(ITestContext context) {
		_log.info("Start test case -- verifyThatUserCanSaveLCTerm" );
		_lcPage.clickLCTermTab();
		String lcType = _excel.getCellValue("lcTerms", 1, 0);
		String lcCurrency = _excel.getCellValue("lcTerms", 1, 1);
		String lcAmount = _excel.getCellValue("lcTerms", 1, 2);
		String lcRules = _excel.getCellValue("lcTerms", 1, 3);
		String expDate = _excel.getCellValue("lcTerms", 1, 4);
		String expCountry = _excel.getCellValue("lcTerms", 1, 5);
		String lcPayment = _excel.getCellValue("lcTerms", 1, 6);
		
		fillApplicationDetails _fillApp = new fillApplicationDetails(_driver);		
		_fillApp.initiateNewLC();
		_fillApp.lcInformation(lcType, lcCurrency, lcAmount,lcRules, expDate, expCountry);
		try {
			Thread.sleep(2000);
			_lcPage.scrollUpPageJS();			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		_lcPage.clickCreditWithBankBICSearch();
		_lcPage.clickCreditBankFirstSearchRecord();
		_fillApp.fillPaymentInfo(lcPayment);     
		
		try {
			Thread.sleep(2000);
			_lcPage.scrollUpPageJS();
			_lcPage.clickSave();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			Assert.assertTrue(_lcPage.getLCAmount().contains(lcAmount));
		}catch(AssertionError ex) {
			 context.setAttribute("error", ex.getMessage());
			    _log.error("Failed assertion verifying LC amount");
			throw ex;
		}
	}
	
	@Test(priority=7,groups= {"smoke"},dependsOnMethods= {"verifyThatUserCanSaveLCTerm"})
	public void verifyThatUserCanSaveShippingDetails(ITestContext context) {
		_log.info("Start test case -- verifyThatUserCanSaveShippingDetails" );
		_lcPage.clickShippingDetailsTab();
		String incoterms = _excel.getCellValue("shipmentDetails", 1, 0);		
		String placeOfReceipt = _excel.getCellValue("shipmentDetails", 1, 3);
		fillApplicationDetails _fillApp = new fillApplicationDetails(_driver);
		_fillApp.initiateNewLC();
		_fillApp.fillShipmentItineries();
		_fillApp.fillIncoterms(incoterms, false);
		try {
			Thread.sleep(2000);
			_lcPage.scrollUpPageJS();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 _lcPage.clickSave();
		 try {
				Thread.sleep(1000);
				_lcPage.scrollUpPageJS();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 try {
				Assert.assertTrue(_lcPage.getPlaceOfReceipt().contains(placeOfReceipt));
			}catch(AssertionError ex) {
				 context.setAttribute("error", ex.getMessage());
				    _log.error("Failed assertion verifying place of receipt");
				throw ex;
			}
	}
	@AfterClass
	public void runLast() {
		_excel.closeExcel();
	}
	
}
