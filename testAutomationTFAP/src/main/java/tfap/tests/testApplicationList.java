package tfap.tests;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import tfap.common.seleniumActions;
import tfap.pages.applicationListPage;
import tfap.utills.listener;
import tfap.utills.takeScreenshots;
import tfap.utills.xray.updateJira;
import tfap.utills.xray.xrayTestResults;

@Listeners(listener.class)
public class testApplicationList extends baseTest {
	WebDriver _driver;
	Logger _log = Logger.getLogger(seleniumActions.class.getName());
	@BeforeClass
	public void init() {
		_driver = this.getDriver();
	}
	
	@Test
	public void testOne(ITestContext context) {
		_log.info("Start test case --" + context.getName());
		context.setAttribute("ID","CFLK-30");
		applicationListPage ap = new applicationListPage(_driver);
		
		_driver.navigate().to("https://www.broadleafcommerce.com/blog/Preventing-Session-Hijacking-With-Spring");
		
		try {
			Assert.assertTrue(_driver.getTitle().contains("Preventing"));
		}catch(AssertionError ex) {
			context.setAttribute("error", ex.getMessage());
			throw ex;
		}
		
		xrayTestResults _xray = new xrayTestResults();
		takeScreenshots _snap = new takeScreenshots();
		_xray.evidanceList(_snap.getBase64Screenshot(_driver), _snap.fileName("CFLK-23"));
		
	}
	
	//@Test(priority=1)
	public void secondTest(ITestContext context) {
		_log.info("Start test case --" + context.getName());
		context.setAttribute("ID","CFLK-95");
		boolean x = false;		
		try {
			Assert.assertTrue(x,"verify that x value is true : ");
		}catch(AssertionError ex) {
			context.setAttribute("error", ex.getMessage());
			throw ex;
		}
	}
	
	

}
