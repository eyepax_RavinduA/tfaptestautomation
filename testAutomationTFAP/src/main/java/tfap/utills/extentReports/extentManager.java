package tfap.utills.extentReports;

import java.util.HashMap;
import java.util.Map;

import com.relevantcodes.extentreports.ExtentReports;

public class extentManager {

	 private static ExtentReports extent;
	    static Map extentTestMap = new HashMap();
	    public synchronized static ExtentReports getReporter(){
	        if(extent ==null){
	            String work_dir =System.getProperty("user.dir");
	            extent = new ExtentReports(work_dir +"\\extentReports\\ExtentReportResults.html",true);
	        }
	        return extent;
	    }
}
