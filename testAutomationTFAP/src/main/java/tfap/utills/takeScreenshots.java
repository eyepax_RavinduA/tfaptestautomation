package tfap.utills;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class takeScreenshots {

	
	public String takeSnap(WebDriver driver, String testCaseID) {
		
		String filePath = "screenshots/"+this.fileName(testCaseID);
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File(filePath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return filePath;
	}
	
	public String getBase64Screenshot(WebDriver driver) {
		 String base64Screenshot =((TakesScreenshot)driver).
	                getScreenshotAs(OutputType.BASE64);
		 return base64Screenshot;
	}
	
	public String fileName(String testCaseID) {
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		return timeStamp+"_"+testCaseID+"_screenshot.png";
	}
}
