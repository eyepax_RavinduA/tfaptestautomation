package tfap.utills.xray;

import org.codehaus.jackson.annotate.JsonProperty;

public class project {
	 private String projectKey;

	 @JsonProperty("key")
	public String getProjectKey() {
		return projectKey;
	}

	public void setProjectKey(String projectKey) {
		this.projectKey = projectKey;
	}
	
}
