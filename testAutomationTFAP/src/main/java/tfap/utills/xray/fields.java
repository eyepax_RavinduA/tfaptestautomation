package tfap.utills.xray;

import org.codehaus.jackson.annotate.JsonProperty;

public class fields {
	
	private project _project;
	private issueType _issueType;
	private description _desc;
	private String summary;
	
	@JsonProperty("project")
	public project get_project() {
		return _project;
	}
	public void set_project(project _project) {
		this._project = _project;
	}
	
	@JsonProperty("summary")
	public String get_summary() {
		return summary;
	}
	
	public void set_summary(String val) {
		summary = val;
	}
	
	@JsonProperty("issuetype")
	public issueType get_issueType() {
		return _issueType;
	}
	public void set_issueType(issueType _issueType) {
		this._issueType = _issueType;
	}
	@JsonProperty("description")
	public description get_desc() {
		return _desc;
	}
	public void set_desc(description _desc) {
		this._desc = _desc;
	}
	
	

}
