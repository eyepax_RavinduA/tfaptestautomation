package tfap.utills.xray;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class innerContent {

	
	private String type,text;
	@JsonProperty("type")
	public String get_type(){
		return type;
	}
	
	public void set_type(String val) {
		type=val;
	}
	@JsonProperty("text")
	public String get_text(){
		return text;
	}
	
	public void set_text(String val) {
		text=val;
	}
	
}
