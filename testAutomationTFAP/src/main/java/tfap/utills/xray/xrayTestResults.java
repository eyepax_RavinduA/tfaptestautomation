package tfap.utills.xray;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class xrayTestResults {

	static JSONObject jObj = new JSONObject();
	   static JSONArray jArray = new JSONArray();

	   public void testExecutionKey(String key,String testPlanKey){

	       jObj.put("testExecutionKey",key);
	       JSONObject jsObj=new JSONObject();
	       jsObj.put("summary","Test Automation Execution");
	       jsObj.put("testPlanKey",testPlanKey);
	       jObj.put("info",jsObj);
	   }


	   public void tests(String key , String tStatus){
	        try{
	            JSONObject json_obj = new JSONObject();
	            json_obj.put("testKey",key);
	            json_obj.put("status",tStatus);
	            jArray.add(json_obj);
	        }catch (NullPointerException ex){System.out.print("test key can not be null" + ex);}
	   }

	   public void addTestResultsToObject(){
	       try{
	           jObj.put("tests",jArray);
	       }catch (NullPointerException ex){System.out.print("Test results can not be null"+ ex);}

	   }

	   public JSONObject getResults(){
	       return jObj;
	   }
	   
	   public JSONObject evidanceList(String baseEncode,String fileName) {
		   JSONObject json_obj = new JSONObject();
		   json_obj.put("data", baseEncode);
		   json_obj.put("filename", fileName);
		   json_obj.put("contentType", "image/jpeg");
		   return json_obj;
		   
	   }

	   
	   public String UpdateTestResults(String token){
	       RestAssured.baseURI="https://xray.cloud.xpand-it.com";
	       RequestSpecification request = RestAssured.given();
	       request.header("Content-Type","application/json");
	       request.header("Authorization", "Bearer "+token);
	       addTestResultsToObject();
	       String result = jObj.toString();
	       request.body(result);
	       Response response = request.post("/api/v1/import/execution");
	       ResponseBody respBody = response.getBody();
	       System.out.print("Status code"+response.statusCode());
	       System.out.print("print job ID "+respBody.jsonPath().get("id").toString());
	       return respBody.asString();
	   }
	
}
