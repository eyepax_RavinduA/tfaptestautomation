package tfap.utills.xray;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import tfap.common.seleniumActions;
import tfap.utills.fileReader;
import tfap.utills.xray.content;
import tfap.utills.xray.description;
import tfap.utills.xray.fields;
import tfap.utills.xray.innerContent;
import tfap.utills.xray.issueType;
import tfap.utills.xray.project;
import tfap.utills.xray.updateJira;

public class createIssue {

	updateJira _jira = new updateJira();
	fields _fd = new fields();
	project _proj = new project();
	issueType _issue = new issueType();
	description _desc = new description();
	content _content = new content();
	innerContent _inner = new innerContent();
	ObjectMapper mapper = new ObjectMapper();
	String jsonInString ;
	public fileReader _config = new fileReader("tfapConfig.properties");
	Logger _log = Logger.getLogger(seleniumActions.class.getName());
	
	public String getIssueJson(String projectKey ,String summary, String description) {
		
		_proj.setProjectKey(projectKey);
		_fd.set_project(_proj);		
		
		_fd.set_summary(summary);
		
		_issue.setIssue("Bug");		
		_fd.set_issueType(_issue);
		
		_desc.set_stype("doc");
		_desc.set_version(1);
		
		_content.setType("paragraph");
		
		_inner.set_type("text");
		_inner.set_text(description);
		
									
		List innerContentList = new ArrayList();		
		innerContentList.add(_inner);		
		_content.setContentlist(innerContentList);		
		
				
		List contentList = new ArrayList();
		contentList.add(_content);
		_desc.setContentlist(contentList);
				
		
		_fd.set_desc(_desc);		
		_jira.setFl(_fd);
		
		try {
			jsonInString = mapper.writeValueAsString(_jira);
			
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return jsonInString;
	}
	
	
	public String createJiraIssue(String projectKey ,String summary, String description) {
		String token = _config.getValue("jiraToken");
		String _json = this.getIssueJson(projectKey, summary, description);
		
		   RestAssured.baseURI=_config.getValue("jira_url");;
	       RequestSpecification request = RestAssured.given();
	       request.header("Content-Type","application/json");
	       request.header("Authorization",token);
	       request.body(_json);
	       Response response = request.post("/rest/api/3/issue");
	       ResponseBody respBody = response.getBody();
	       int statusCode = response.statusCode();
	       if(statusCode!=201) {
	    	   _log.info("Bug has not created and request has been failed with"+ statusCode);
	       }
	       
	       String jira_id = respBody.jsonPath().get("key").toString();
	       
	       return jira_id;
		
	}
	
	
	
	
}
