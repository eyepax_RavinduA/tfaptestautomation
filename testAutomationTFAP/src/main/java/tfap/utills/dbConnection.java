package tfap.utills;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class dbConnection {
	
	Connection con ;
	Statement _stmt;
	ResultSet _rset;
	public fileReader _locator = new fileReader("tfapConfig.properties");
	public void setupConnection() {
		
		try {
			con = DriverManager.getConnection(_locator.getValue("dbCon"), _locator.getValue("dbUser"), _locator.getValue("dbPwd"));
			System.out.println("Connection sucess");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Connection failed");
		}
		
		try {
			_stmt = con.createStatement();
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		
	}
	
	public ResultSet getData(String query) {
		
		this.setupConnection();
		try {
			_rset = _stmt.executeQuery(query);
			return _rset;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;			
		}
		
		
	}
	

}
