package tfap.utills;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;
import tfap.tests.*;

import com.relevantcodes.extentreports.LogStatus;

import tfap.utills.extentReports.extentManager;
import tfap.utills.extentReports.extentTestManager;

public class listener implements ITestListener, ISuiteListener, IInvokedMethodListener {
	
	 static ITestResult test_results;
	    private static String getTestMethodName(ITestResult iTestResult) {
	        return iTestResult.getMethod().getConstructorOrMethod().getName();
	    }
	    
	    
	    public void onStart(ISuite arg0) {

	        Reporter.log("About to begin executing Suite " + arg0.getName(), true);

	    }

	    // This belongs to ISuiteListener and will execute, once the Suite is finished

	    
	    public void onFinish(ISuite arg0) {

	        Reporter.log("About to end executing Suite " + arg0.getName(), true);

	    }

	    // This belongs to ITestListener and will execute before starting of Test set/batch

	    public void onStart(ITestContext arg0) {

	        Reporter.log("About to begin executing Test " + arg0.getName(), true);
	    }

	    // This belongs to ITestListener and will execute, once the Test set/batch is finished

	    public void onFinish(ITestContext arg0) {

	        Reporter.log("Completed executing test " + arg0.getName(), true);
	        Reporter.log("Test case ID is "+arg0.getAttribute("ID"),true);
	        extentTestManager.endTest();
	        extentManager.getReporter().flush();

	    }

	    // This belongs to ITestListener and will execute only when the test is pass

	    public void onTestSuccess(ITestResult arg0) {

	        // This is calling the printTestResults method

	        printTestResults(arg0);
	        extentTestManager.getTest().log(LogStatus.PASS, "Test passed");
	        test_results = arg0;
	    }

	    // This belongs to ITestListener and will execute only on the event of fail test

	    public void onTestFailure(ITestResult arg0) {

	        // This is calling the printTestResults method

	        printTestResults(arg0);

	        //Get driver from BaseTest and assign to local webdriver variable.
	        Object testClass = arg0.getInstance();
	        WebDriver webDriver = ((baseTest) testClass).getDriver();

	        //Take base64Screenshot screenshot.
	        String base64Screenshot = "data:image/png;base64,"+((TakesScreenshot)webDriver).
	                getScreenshotAs(OutputType.BASE64);

	        //Extentreports log and screenshot operations for failed tests.
	        extentTestManager.getTest().log(LogStatus.FAIL,"Test Failed",
	                extentTestManager.getTest().addBase64ScreenShot(base64Screenshot));
	       
	        test_results = arg0;
	    }

	    // This belongs to ITestListener and will execute before the main test start (@Test)

	    public void onTestStart(ITestResult arg0) {

	        System.out.println("The execution of the main test starts now");
	        extentTestManager.startTest(arg0.getMethod().getMethodName(),"");
	    }

	    // This belongs to ITestListener and will execute only if any of the main test(@Test) get skipped

	    public void onTestSkipped(ITestResult arg0) {

	        printTestResults(arg0);
	        extentTestManager.getTest().log(LogStatus.SKIP, "Test Skipped");
	    }

	    public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {

	    }

	    // This is the method which will be executed in case of test pass or fail

	    // This will provide the information on the test

	    private void printTestResults(ITestResult result) {

	        Reporter.log("Test Method resides in " + result.getTestClass().getName(), true);

	        if (result.getParameters().length != 0) {

	            String params = null;

	            for (Object parameter : result.getParameters()) {

	                params += parameter.toString() + ",";

	            }

	            Reporter.log("Test Method had the following parameters : " + params, true);

	        }

	        String status = null;

	        switch (result.getStatus()) {

	            case ITestResult.SUCCESS:

	                status = "Pass";

	                break;

	            case ITestResult.FAILURE:

	                status = "Failed";

	                break;

	            case ITestResult.SKIP:

	                status = "Skipped";

	        }

	        Reporter.log("Test Status: " + status, true);

	    }

	    // This belongs to IInvokedMethodListener and will execute before every method including @Before @After @Test

	    public void beforeInvocation(IInvokedMethod arg0, ITestResult arg1) {

	        String textMsg = "About to begin executing following method : " + returnMethodName(arg0.getTestMethod());

	        Reporter.log(textMsg, true);

	    }

	    // This belongs to IInvokedMethodListener and will execute after every method including @Before @After @Test

	    public void afterInvocation(IInvokedMethod arg0, ITestResult arg1) {

	        String textMsg = "Completed executing following method : " + returnMethodName(arg0.getTestMethod());

	        Reporter.log(textMsg, true);

	    }

	    // This will return method names to the calling function

	    private String returnMethodName(ITestNGMethod method) {

	        return method.getRealClass().getSimpleName() + "." + method.getMethodName();

	    }

	    public ITestResult returnTestResult(){
	        return test_results;
	    }

}
