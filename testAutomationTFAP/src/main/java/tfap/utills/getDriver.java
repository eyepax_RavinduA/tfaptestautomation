package tfap.utills;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.browserstack.local.Local;

public class getDriver {

	
	 private Local l;
	    private WebDriver driver;
	    public WebDriver getLocalWebDriver(String browser){
	        String _browser = browser.toLowerCase();
	        switch (_browser)
	        {
	            case "chrome":
	                System.setProperty("webdriver.chrome.driver","src/main/resources/drivers/chromedriver.exe");
	                ChromeOptions option = new ChromeOptions();
	                //option.addArguments("--headless");
	                driver = new ChromeDriver(option);
	                break;
	            case "ie":
	                break;
	            case "firefox":

	                break;
	        }
	        return driver;
	    }


	    public WebDriver getBrowserStack(String config_file, String environment) throws IOException, ParseException {
	        JSONParser parser = new JSONParser();
	        JSONObject config = (JSONObject) parser.parse(new FileReader("src/main/resources/conf/" + config_file));
	        JSONObject envs = (JSONObject) config.get("environments");

	        DesiredCapabilities capabilities = new DesiredCapabilities();

	        Map<String, String> envCapabilities = (Map<String, String>) envs.get(environment);
	        Iterator it = envCapabilities.entrySet().iterator();
	        while (it.hasNext()) {
	            Map.Entry pair = (Map.Entry)it.next();
	            capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
	        }

	        Map<String, String> commonCapabilities = (Map<String, String>) config.get("capabilities");
	        it = commonCapabilities.entrySet().iterator();
	        while (it.hasNext()) {
	            Map.Entry pair = (Map.Entry)it.next();
	            if(capabilities.getCapability(pair.getKey().toString()) == null){
	                capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
	            }
	        }

	        String username = System.getenv("BROWSERSTACK_USERNAME");
	        if(username == null) {
	            username = (String) config.get("user");
	        }

	        String accessKey = System.getenv("BROWSERSTACK_ACCESS_KEY");
	        if(accessKey == null) {
	            accessKey = (String) config.get("key");
	        }

	        String app = System.getenv("BROWSERSTACK_APP_ID");
	        if(app != null && !app.isEmpty()) {
	            capabilities.setCapability("app", app);
	        }

	        if(capabilities.getCapability("browserstack.local") != null && capabilities.getCapability("browserstack.local") == "true"){
	            l = new Local();
	            Map<String, String> options = new HashMap<String, String>();
	            options.put("key", accessKey);
	            try {
	                l.start(options);
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        }

	        driver = new RemoteWebDriver(new URL("http://"+username+":"+accessKey+"@"+config.get("server")+"/wd/hub"), capabilities);


	        return null;
	    }
	
}
