package tfap.utills;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.formula.functions.Column;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import tfap.tests.baseTest;


public class readExcel {

	String _fileName;
	Logger _log = Logger.getLogger(readExcel.class.getName());
	OPCPackage pkg = null;
	public fileReader _prop = new fileReader("tfapConfig.properties");
	
	public readExcel(String fileName) {
		_fileName = _prop.getValue("excelPath")+fileName+".xlsx";		
	}	
	
	public Sheet getExcelSheet(String sheetName) {
		//_fileName ="src/main/resources/testData/LCIssuanceTestData.xlsx";		 
		try {
			pkg = OPCPackage.open(new File(_fileName));
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		XSSFWorkbook wb = null;
		  try {
			 wb = new XSSFWorkbook(pkg);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  Sheet _sheet = null;
		 if(wb!=null) {
			  _sheet = wb.getSheet(sheetName);
		 }
		 return _sheet;
	}
	
	
	public String getCellValue(String sheetName , int row,int col ) {	
		DataFormatter dataFormatter = new DataFormatter();
		Sheet _sheet = this.getExcelSheet(sheetName);
		Cell _cell = _sheet.getRow(row).getCell(col);
		if(_cell==null) {
			_log.error("Cell is empty");
			return null;
		}
		String cellValue = dataFormatter.formatCellValue(_cell);		
		return cellValue;
	}
	
	public void closeExcel() {
		if(pkg!=null) {
			try {
				pkg.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
