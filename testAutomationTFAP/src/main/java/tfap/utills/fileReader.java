package tfap.utills;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class fileReader {

	
	private Properties properties;
    
    public fileReader(String fileName) {
    	BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(fileName));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + fileName);
        }
    }
    
    

    public String getValue(String keyVal){
        return properties.getProperty(keyVal);
    }
}
