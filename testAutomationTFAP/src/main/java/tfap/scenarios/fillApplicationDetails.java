package tfap.scenarios;

import java.util.Date;

import org.openqa.selenium.WebDriver;

import tfap.pages.lcPage;
import tfap.utills.readExcel;

public class fillApplicationDetails {
	
	WebDriver _driver;
	lcPage _lcApp;
	public fillApplicationDetails(WebDriver driver) {
		_driver = driver;
	}
	
	public void initiateNewLC() {
		_lcApp = new lcPage(_driver);
	}
	
	public void fillPrimaryContactPersonDetails(String name,String number, String email) {
		
		if(name!=null) {
			_lcApp.setPrimaryContactName(name);
		}		
		if(number!=null) {
			_lcApp.setPrimaryContactNumber(number);
		}
		if(email!=null) {
			_lcApp.setPrimaryContactEmail(email);
		}
	}
	
	public void fill3rdPartyInformation(Boolean state,String companyName, String countryCode,String Adline1 , String Adline2 , String Adline3) {
		if(state) {
			
			if(companyName!= null) {
				_lcApp.set3rdPartyCompanyName(companyName);
			}
			if(countryCode!= null) {
				_lcApp.set3rdPartyCountryName(countryCode);
			}			
			if(Adline1!= null) {
				_lcApp.set3rdPartyCompanyAddressLine1(Adline1);
			}
			if(Adline2!= null) {
				_lcApp.set3rdPartyCompanyAddressLine2(Adline2);
			}
			if(Adline3!= null) {
				_lcApp.set3rdPartyCompanyAddressLine3(Adline3);
			}
		}
	}
	
	public void fillNewBeneficiary(String name,String Adline1, String Adline2, String Adline3, String City, String CountryCode,String ContactNo,String Email, String PersonName) {
		
		if(name!=null) {
			_lcApp.setBeneficiaryName(name);
		}
		if(Adline1!=null) {
			_lcApp.setBeneficiaryAddressLine1(Adline1);
		}
		if(Adline2!=null) {
			_lcApp.setBeneficiaryAddressLine2(Adline2);
		}
		if(Adline3!=null) {
			_lcApp.setBeneficiaryAddressLine3(Adline3);
		}
		if(City!=null) {
			_lcApp.setBeneficiaryCity(City);
		}
		if(CountryCode!=null) {
			_lcApp.setBeneficiaryCountryCode(CountryCode);
		}
		if(ContactNo!=null) {
			_lcApp.setBeneficiaryContactNumber(ContactNo);
		}
		if(Email!=null) {
			_lcApp.setBeneficiaryContactEmail(Email);
		}
		if(PersonName!=null) {
			_lcApp.setBeneficiaryContactName(PersonName);
		}
	}
	
	public void lcInformation(String lcTye,String ccy,String amount,String lcRules,String expDate,String expCountry ) {
		if(lcTye!=null) {
			_lcApp.setLCType(lcTye);
			if(lcTye.contains("BackToBack")) {
				readExcel _excel = new readExcel("LCIssuanceTestData");
				String number = _excel.getCellValue("lcTerm", 1, 7);
				_lcApp.setBackToBackLCNo(number);
			}
		}
		if(ccy!=null) {
			_lcApp.setCurrency(ccy);
		}
		if(amount!=null) {
			_lcApp.setLCamount(amount);
		}
		if(lcRules!=null) {
			_lcApp.setApplicableRule(lcRules);
		}
		if(expDate!=null) {
			this.setDate();
		}
		if(expCountry!=null) {
			_lcApp.setExpiryCountry(expCountry);
		}
	}
	
	private void fillDraweeDetails() {
		readExcel _excel = new readExcel("LCIssuanceTestData");
		String draftAt = _excel.getCellValue("payment", 1, 0);
		String draftTerm = _excel.getCellValue("payment", 1, 1);
		if(draftAt!=null) {
			_lcApp.setDraftAt(draftAt);
		}
		if(draftTerm!=null) {
			_lcApp.setDraftAtTerm(draftTerm);
		}
		_lcApp.clickDraweeBICSearchBtn();
		_lcApp.clickFirstSearchRecord();	
		
		
	}
	private void setPaymentIstruction(String instruction) {
		if(instruction!=null) {
			_lcApp.setPaymentInstruction(instruction);
		}
	}
	
	public void fillPaymentInfo(String creditAvailable) {
		readExcel _excel = new readExcel("LCIssuanceTestData");
		String presentationPeriod = _excel.getCellValue("payment", 1, 3);
		String sendBy =_excel.getCellValue("payment", 1, 5);
		String instruction =_excel.getCellValue("payment", 1, 7);
		String sendByOther = _excel.getCellValue("payment", 1, 6);
		
		if(creditAvailable!=null) {
			
			if(creditAvailable.toUpperCase()=="BY NEGOTIATION") {
				_lcApp.setPayment(creditAvailable);
				this.setPaymentIstruction(instruction);
				this.fillDraweeDetails();
			}
			else if(creditAvailable.toUpperCase()=="BY ACCEPTANCE") {
				_lcApp.setPayment(creditAvailable);
				this.fillDraweeDetails();
			}
			else if(creditAvailable.toUpperCase()=="BY DEF PAYMENT") {
				_lcApp.setPayment(creditAvailable);
				this.setPaymentIstruction(instruction);
			}
			else if(creditAvailable.toUpperCase()=="BY MIXED PYMT") {
				this.setPaymentIstruction(instruction);
				this.fillDraweeDetails();
			}
			else if(creditAvailable.toUpperCase()=="BY PAYMENT") {
				_lcApp.setPayment(creditAvailable);
			}
			else {
				System.out.println("Ivalid payment type");
			}
		}
		
		if(presentationPeriod!=null) {
			_lcApp.setPeriodOfPresentationDays(presentationPeriod);
		}
		if(sendBy!=null) {
			_lcApp.setPaymentSendBy(sendBy);
			if(sendBy.toLowerCase().contains("others")) {
				_lcApp.setSendByInstruction(sendByOther);
			}
		}
	}
	
	public void fillShipmentItineries() {
		readExcel _excel = new readExcel("LCIssuanceTestData");
		String placeOfReceipt = _excel.getCellValue("shipmentDetails", 1, 3);
		String loadingPort = _excel.getCellValue("shipmentDetails", 1, 4);
		String loadingCountry = _excel.getCellValue("shipmentDetails", 1, 5);
		String discharginPort = _excel.getCellValue("shipmentDetails", 1, 6);
		String dischargingCountry = _excel.getCellValue("shipmentDetails", 1, 7);
		String finalDestination = _excel.getCellValue("shipmentDetails", 1, 8);
		String periodOfShipment = _excel.getCellValue("shipmentDetails", 1, 9);
		String latestShipmentDate = _excel.getCellValue("shipmentDetails", 1, 10);
		String descriptionOfGoods = _excel.getCellValue("shipmentDetails", 1, 11);
		
		if(placeOfReceipt!=null) {
			_lcApp.setPlaceOfReceipt(placeOfReceipt);
		}
		if(loadingPort!=null) {
			_lcApp.setPortOfLording(loadingPort);
		}
		if(loadingCountry!=null) {
			_lcApp.setLoadingPortCountry(loadingCountry);
		}
		if(discharginPort!=null) {
			_lcApp.setPortOfDischarge(discharginPort);
		}
		if(dischargingCountry!=null) {
			_lcApp.setDischargePortCountry(dischargingCountry);
		}
		if(finalDestination!=null) {
			_lcApp.setFinalDestination(finalDestination);
		}
		if(periodOfShipment!=null) {
			_lcApp.setShipmentPeriod(periodOfShipment);
		}
		if(latestShipmentDate!=null) {
			_lcApp.setLatestShipmentDate(latestShipmentDate);
		}
		if(descriptionOfGoods!=null) {
			_lcApp.setDescriptionOfGoodsAndServices(descriptionOfGoods);
		}
	}
	
	public void fillIncoterms(String incoterms,boolean other) {
		if(incoterms!=null) {
			if(incoterms.toLowerCase().contains("oth")) {
				_lcApp.setIncoterms("OTH");
			}
			
			else if(incoterms.toLowerCase().contains("exw")) {
				_lcApp.setIncoterms("EXW");
			}
			else {
			   _lcApp.setIncoterms(incoterms);			   
			}
			
		}
		if(other) {
			_lcApp.setSpecifyOtherInsuranceClause("Test data input from automation");
		}
		
	}
	
	public void setDate() {
		_lcApp.openCalendar();
		try {
			Thread.sleep(1000);			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		_lcApp.clickNextMonth();		
		Date _date = new Date();
	    int currentDate=_date.getDate();
		_lcApp.clickDate(currentDate);
	}

}
