package tfap.scenarios;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import tfap.common.seleniumActions;
import tfap.pages.applicationListPage;
import tfap.pages.lcPage;
import tfap.utills.fileReader;

public class pageNavigation extends seleniumActions {

	public fileReader _locater = new fileReader("src/main/resources/locators/pageLocators.properties");
    WebDriver _driver;
    
    public pageNavigation(WebDriver driver) {
    	super(driver);
    	_driver = driver;
    }
    
    public applicationListPage navigateToApplicationList() {
    	this.clickElement(By.xpath(_locater.getValue("applicationList")),15);
    	return new applicationListPage(_driver);
    }
    
    public lcPage navigateToNewLCApplication() {
    	this.clickElement(By.id(_locater.getValue("newApp")), 5);
    	return new lcPage(_driver);
    }
    
    public void scrollUpPage() {
		
		this.scrollUpPageJS();
	}
}
