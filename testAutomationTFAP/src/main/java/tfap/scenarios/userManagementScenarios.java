package tfap.scenarios;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import tfap.pages.applicationListPage;
import tfap.pages.dashboardPage;
import tfap.pages.loginPage;
import tfap.tests.testLcIssuance;

public class userManagementScenarios {
	
	WebDriver _driver;
	public userManagementScenarios(WebDriver driver) {
		_driver= driver;
	}
	Logger _log = Logger.getLogger(userManagementScenarios.class.getName());
	
	public dashboardPage loginToApplication(String userName,String password) {
		loginPage _lpage = new loginPage(_driver);
		_lpage.setUserName(userName);		
		_lpage.setPassword(password);
		_lpage.clickLoginButton();
		_log.info("Set username and passwrod and clicked on login");
		return new dashboardPage(_driver);
	}
	

}
