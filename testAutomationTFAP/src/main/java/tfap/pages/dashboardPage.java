package tfap.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import tfap.common.seleniumActions;
import tfap.utills.fileReader;

public class dashboardPage extends seleniumActions{

	WebDriver _driver;
	public fileReader _locator = new fileReader("src/main/resources/locators/dashboardPageLocators.properties");
	
	public dashboardPage(WebDriver driver) {
		super(driver);
		_driver = driver;
	}
	
	public String getLoggedInUserName() {
		return  this.getElementText(By.xpath(_locator.getValue("userName")), 5)  ;
	}
	
}
