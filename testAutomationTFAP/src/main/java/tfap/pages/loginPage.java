package tfap.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import tfap.common.seleniumActions;
import tfap.utills.fileReader;

public class loginPage extends seleniumActions {

	public fileReader _lcapp = new fileReader("src/main/resources/locators/loginPageLocators.properties");
	WebDriver _driver;
	
	public loginPage(WebDriver driver) {
		super(driver);
		_driver = driver;
	}
	
	public void setUserName(String userName) {
		this.setValue(By.id(_lcapp.getValue("userName")), userName, 5);
	}
	public void setPassword(String password) {
		this.setValue(By.id(_lcapp.getValue("password")), password, 5);
	}
	public void clickLoginButton() {
		this.clickElement(By.name(_lcapp.getValue("loginBtn")));
	}	
	
}
