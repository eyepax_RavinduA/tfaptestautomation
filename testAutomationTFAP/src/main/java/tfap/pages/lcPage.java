package tfap.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import tfap.common.seleniumActions;
import tfap.utills.fileReader;

public class lcPage extends seleniumActions {
	
	WebDriver _driver;
	public fileReader _lcapp = new fileReader("src/main/resources/locators/lcPageLocators.properties");
	
	public lcPage(WebDriver driver) {
		super(driver);
		_driver = driver;
	}

//Applicant Details Tab
public void setCompanyRef(String ref) {
	this.setValue(By.id(_lcapp.getValue("companyRef")), ref, 5);
	
}
public void setPrimaryContactName(String contactName) {
	
	this.setValue(By.id(_lcapp.getValue("contactPersonName")), contactName, 5);
}

public void setPrimaryContactNumber(String contactNumber) {
	this.setValue(By.id(_lcapp.getValue("contactNumber")), contactNumber, 5);
}

public void setPrimaryContactEmail(String contactEmail) {
	this.setValue(By.id(_lcapp.getValue("contactEmail")), contactEmail, 5);
}
public void set3rdPartyCompanyName(String companyName) {
	this.setValue(By.id(_lcapp.getValue("3rdPartyCompanyName")), companyName, 10);
}
public void set3rdPartyCountryName(String country) {
	this.setDropdownValueByVal(By.id(_lcapp.getValue("3rdPartyCountryName")), country, 10);
}

public void set3rdPartyCompanyAddressLine1(String addressline1) {
	this.setValue(By.id(_lcapp.getValue("3rdPartyAdline1")), addressline1, 5);
}

public void set3rdPartyCompanyAddressLine2(String addressline2) {
	this.setValue(By.xpath(_lcapp.getValue("3rdPartyAdline2")), addressline2, 5);
}

public void set3rdPartyCompanyAddressLine3(String addressline3) {
	this.setValue(By.xpath(_lcapp.getValue("3rdPartyAdline3")), addressline3, 5);
}

public void clickSave() {	
	this.clickElement(By.id(_lcapp.getValue("saveBtn")),15);
}

public void clickBeneficiaryTab() {
	this.clickElement(By.id(_lcapp.getValue("beneficiaryTab")));
}

public String getActiveNewStep() {
	return this.getElementText(By.xpath(_lcapp.getValue("newState")), 5);
}
	
public String getActiveStep() {
	return this.getElementText(By.xpath(_lcapp.getValue("activeStep")), 5);
}

public Boolean getVisibilityOfApplicationTab() {
	return this.getVisibilityOfElement(By.id(_lcapp.getValue("lcApplicationFormTab")), 5);
}

public String getPersonNameErrorMessage() {
	return this.getElementText(By.id(_lcapp.getValue("errorPersonName")), 5);
}

public String getContactNumberErrorMessage() {
	return this.getElementText(By.id(_lcapp.getValue("errorContactNumber")), 5);
}

public String get3rdPartyCompanyNameErrorMessage() {
	return this.getElementText(By.id(_lcapp.getValue("error3rdPartyCompanyName")), 5);
}

public String getAlertErrorMessage() {
	return this.getElementText(By.xpath(_lcapp.getValue("alertErrorMsg")), 5);
}

public String getTFApplicationId() {
	return this.getElementText(By.xpath(_lcapp.getValue("tfApplicationId")), 5);
}
public void click3rdPartyYes() {
	this.clickElement(By.xpath(_lcapp.getValue("3rdPartyToggleYesBtn")));
}

// Beneficiary Tab
public String getBeneficiaryHeadings(){
	return this.getElementText(By.xpath(_lcapp.getValue("benHeadings")), 5);
}
public void clickSearchBeneficiary() {
	this.clickElement(By.id(_lcapp.getValue("benSearchBtn")),15);
}

public void clickAddBeneficiary() {
	this.clickElement(By.xpath(_lcapp.getValue("benAddBtn")));
}

public void setBeneficiaryName(String name) {	
	this.setValue(By.xpath(_lcapp.getValue("benName")), name, 5);
}

public void setBeneficiaryContactName(String contactName) {	
	this.setValue(By.xpath(_lcapp.getValue("benPopupName")), contactName, 5);
}

public void setBeneficiaryContactNumber(String contactNumber) {
	this.setValue(By.xpath(_lcapp.getValue("benPopupContactNumber")), contactNumber, 5);
}

public void setBeneficiaryContactEmail(String contactEmail) {
	this.setValue(By.xpath(_lcapp.getValue("benPopupEmail")), contactEmail, 5);
}

public void setBeneficiaryAddressLine1(String addressline1) {
	this.setValue(By.xpath(_lcapp.getValue("benPopupAdline1")), addressline1, 5);
}

public void setBeneficiaryAddressLine2(String addressline2) {
	this.setValue(By.xpath(_lcapp.getValue("benPopupAdline2")), addressline2, 5);
}

public void setBeneficiaryAddressLine3(String addressline3) {
	this.setValue(By.xpath(_lcapp.getValue("benPopupAdline3")), addressline3, 5);
}

public void setBeneficiaryCity(String city) {	
	this.setValue(By.xpath(_lcapp.getValue("benCity")), city, 5);
}

public void setBeneficiaryCountryCode(String code) {	
	this.setValue(By.xpath(_lcapp.getValue("benCountryCode")), code, 5);
}

public void setBeneficiaryOtherInfo(String otherInfo) {
	this.setValue(By.xpath(_lcapp.getValue("benOtherInfo")), otherInfo, 5);
}

public void setBeneficiarySearch(String name) {
	this.setValue(By.id(_lcapp.getValue("searchText")), name, 5);
}

public void clickFirstSearchRecord() {
	this.clickElement(By.xpath(_lcapp.getValue("fistSearchRecord")),10);
}

public void clickCreditBankFirstSearchRecord() {
	this.clickElement(By.xpath(_lcapp.getValue("lcFirstCreditBankName")),10);
}
public String getBeneficiaryName() {
	return this.getElementAttributeText(By.id(_lcapp.getValue("benName")), "value", 10);
}

public String getBeneficiaryContactNumber() {
	return this.getElementText(By.xpath(_lcapp.getValue("benContactNumber")), 5);
}

public String getBeneficiaryContactPersonName() {
	return this.getElementText(By.xpath(_lcapp.getValue("benContactPersonName")), 5);
}

public String getBeneficiaryEmail() {
	return this.getElementText(By.xpath(_lcapp.getValue("benContactEmail")), 5);
}

public String getBeneficiaryAdLine1() {
	return this.getElementText(By.xpath(_lcapp.getValue("benAdline1")), 5);
}

public String getBeneficiaryAdLine2() {
	return this.getElementText(By.xpath(_lcapp.getValue("benAdline2")), 5);
}

public String getBeneficiaryAdLine3() {
	return this.getElementText(By.xpath(_lcapp.getValue("benAdline3")), 5);
}

public void clickSaveBeneficiary() {
	this.clickElement(By.xpath(_lcapp.getValue("benPopupSave")));
}

public void closeBeneficiaryPopUp() {
	this.clickElement(By.xpath(_lcapp.getValue("benPopupClose")));
}

//Beneficiary Search Pop up

public void setBeneficiarySearchName(String name) {	
	this.setValue(By.xpath(_lcapp.getValue("benName")), name, 5);
}

public void closeSearchPopup() {
	this.clickElement(By.xpath(_lcapp.getValue("benSearchPopupClose")));
}

//LC Terms tab
public void clickLCTermTab() {
	this.clickElement(By.id(_lcapp.getValue("lcTermTab")),5);
}


public void setLCType(String lcType) {
	this.setDropdownValueByText(By.id(_lcapp.getValue("lcType")), lcType, 10);
}
public void setBackToBackLCNo(String number) {	
	this.setValue(By.id(_lcapp.getValue("backToBackNo")), number, 5);
}
public void setCurrency(String currency) {
	this.setDropdownValueByText(By.id(_lcapp.getValue("currency")), currency, 10);
}

public void setLCamount(String amount) {	
	this.setValue(By.id(_lcapp.getValue("lcAmount")), amount, 5);
}
public void setSendByInstruction(String ins) {
	this.setValue(By.id(_lcapp.getValue("lcSendByOtherInstruction")), ins,5);
}
public String getLCAmount() {
	return this.getElementAttributeText(By.id(_lcapp.getValue("lcAmount")), "value", 10);
}

public void setTolerancePercentage(String percentage) {	
	this.setValue(By.xpath(_lcapp.getValue("lcTolerancePercentage")), percentage, 5);
}

public void setToleranceAmount(String amount) {	
	this.setValue(By.xpath(_lcapp.getValue("lcToleranceAmount")), amount, 5);
}

public void setApplicableRule(String applicableRule) {
	this.setDropdownValueByText(By.id(_lcapp.getValue("lcApplicableRule")), applicableRule, 10);
}
public void setExpiryDate(String expDate) {	
	this.setValue(By.id(_lcapp.getValue("lcExpiryDate")), expDate, 5);
}
public void setExpiryPlace(String expPlace) {	
	this.setValue(By.xpath(_lcapp.getValue("lcExpiryPlace")), expPlace, 5);
}

public void setExpiryCountry(String expCountry) {
	this.setDropdownValueByVal(By.id(_lcapp.getValue("lcExpiryCountry")), expCountry, 10);
}

public void clickCreditBankSearch() {
	this.clickElement(By.id(_lcapp.getValue("lcCreditSearchBtn")), 5);
}

public void setBankNameCreditSearch(String bankName) {
	this.setValue(By.xpath(_lcapp.getValue("lcCreditBankName")), bankName, 5);
}

public void setBankBICCreditSearch(String bankBIC) {
	this.setValue(By.xpath(_lcapp.getValue("lcCreditBankBIC")), bankBIC, 5);
}

public void clickCreditWithBankBICSearch() {
	this.clickElement(By.id(_lcapp.getValue("lcCreditBICBtn")), 5);
}

public void setBankAdLine1(String adline1) {
	this.setValue(By.xpath(_lcapp.getValue("lcCreditBankAdline1")), adline1, 5);
}

public void setBankAdLine2(String adline2) {
	this.setValue(By.xpath(_lcapp.getValue("lcCreditBankAdline2")), adline2, 5);
}

public void setBankAdLine3(String adline3) {
	this.setValue(By.xpath(_lcapp.getValue("lcCreditBankAdline3")), adline3, 5);
}

public void setPayment(String paymentInfo) {
	this.setDropdownValueByVal(By.id(_lcapp.getValue("lcPaymentDrop")), paymentInfo, 5);
}

public void setPeriodOfPresentationDays(String days) {
	this.setValue(By.id(_lcapp.getValue("lcPaymentPeriodInDays")), days, 5);
}

public void setPaymentTerms(String terms) {
	this.setDropdownValueByVal(By.id(_lcapp.getValue("lcPaymentTerms")), terms, 5);
}

public void setPaymentSendBy(String sendBy) {
	this.setDropdownValueByVal(By.id(_lcapp.getValue("lcPaymentSendBy")), sendBy, 5);
}

public void clickTransaferable() {
	this.clickElement(By.xpath(_lcapp.getValue("lcPaymentTransferableBtn")), 5);
}

public void setDraftAt(String draft) {
	this.setValue(By.id(_lcapp.getValue("lcPaymentDraftAt")), draft, 5);
}

public void setDraftAtTerm(String draftTerm) {
	this.setDropdownValueByVal(By.id(_lcapp.getValue("lcPaymentDraftTerm")), draftTerm, 5);
}

public void clickDraweeBICSearchBtn() {
	this.clickElement(By.id(_lcapp.getValue("lcPaymentDraweeBICSearchBtn")), 5);
}

public void setDraweeBICSearch(String drawee) {
	this.setValue(By.xpath(_lcapp.getValue("lcPaymentDraweeBICSearch")), drawee, 5);
}

public void clickDraweeNameSearchBtn() {
	this.clickElement(By.xpath(_lcapp.getValue("lcPaymentDraweeBICSearchBtn")), 5);
}

public void setDraweeAdline1(String adline1) {
	this.setValue(By.xpath(_lcapp.getValue("lcPaymentDraweeAdline1")), adline1, 5);
}

public void setDraweeAdline2(String adline2) {
	this.setValue(By.xpath(_lcapp.getValue("lcPaymentDraweeAdline2")), adline2, 5);
}

public void setDraweeAdline3(String adline3) {
	this.setValue(By.xpath(_lcapp.getValue("lcPaymentDraweeAdline3")), adline3, 5);
}

public void setPaymentInstruction(String paymentInstruction) {
	this.setValue(By.id(_lcapp.getValue("lcPaymentInstruction")), paymentInstruction, 5);
}

public void setAdditionalCondition(String adCondition) {
	this.setValue(By.id(_lcapp.getValue("lcAdditionalConditions")), adCondition, 5);
}

public void checkAllChargersForApplicantAccount() {
	this.clickElement(By.id(_lcapp.getValue("lcChargesForApplicantsAccount")), 5);
}
public void checkChargersForAgent() {
	this.clickElement(By.id(_lcapp.getValue("lcChargesForAgent")), 5);
}
public void checkChargersForBankCommission() {
	this.clickElement(By.id(_lcapp.getValue("lcChargesForBankCommission")), 5);
}
public void checkChargersForBankCorrespondingCommision() {
	this.clickElement(By.id(_lcapp.getValue("lcChargesForBankCorrespondent")), 5);
}
public void checkChargersForCommercialDiscount() {
	this.clickElement(By.id(_lcapp.getValue("lcChargesForCommercialDiscount")), 5);
}
public void checkChargersForInsurancePremium() {
	this.clickElement(By.id(_lcapp.getValue("lcChargesForInsurancePremium")), 5);
}
public void checkChargersForBankPostage() {
	this.clickElement(By.id(_lcapp.getValue("lcChargesForBankPostage")), 5);
}
public void checkChargersForStampDuty() {
	this.clickElement(By.id(_lcapp.getValue("lcChargesForStampDuty")), 5);
}
public void checkChargersForTeletransmissionCharges() {
	this.clickElement(By.id(_lcapp.getValue("lcChargesForTeletransmission")), 5);
}
public void checkChargersForWarehouse() {
	this.clickElement(By.id(_lcapp.getValue("lcChargesForWarehouse")), 5);
}
public void checkChargersForOther() {
	this.clickElement(By.id(_lcapp.getValue("lcChargesForOthers")), 5);
}

public void openCalendar() {
	this.scrollToElement(By.id(_lcapp.getValue("lcApplicableRule")), 5);
	this.javaScriptClick("id",_lcapp.getValue("lcExpiryDate"));
}
public void clickNextMonth() {
	this.clickElement(By.xpath(_lcapp.getValue("calenderNextBtn")), 5);
}
public void clickDate(int date) {
	this.clickElement(By.xpath(_lcapp.getValue("calenderDate")+date+"')]"), 5);
}
// Shipment details
public void clickShippingDetailsTab() {
	this.clickElement(By.id(_lcapp.getValue("lcShipmentTab")),5);
}


public void clickPartialShipmentAllowed() {
	this.clickElement(By.xpath(_lcapp.getValue("lcPartialShipmentAllowed")), 5);
}
public void clickPartialShipmentNotAllowed() {
	this.clickElement(By.xpath(_lcapp.getValue("lcPartialShipmentNotAllowed")), 5);
}
public void setPlaceOfReceipt(String place) {
	this.setValue(By.id(_lcapp.getValue("lcPlaceOfReceipt")), place, 5);
}
public void setPortOfLording(String lordingPlace) {
	this.setValue(By.id(_lcapp.getValue("lcPlaceOfLoading")), lordingPlace, 5);
}
public void setPortOfDischarge(String dischargePlace) {
	this.setValue(By.id(_lcapp.getValue("lcPlaceOfDischarge")), dischargePlace, 5);
}
public void setLoadingPortCountry(String val) {
	this.setDropdownValueByVal(By.id(_lcapp.getValue("lcPortLoadingCountry")), val, 5);
}
public void setDischargePortCountry(String val) {
	this.setDropdownValueByVal(By.id(_lcapp.getValue("lcPortDischargeCountry")), val, 5);
}
public void setFinalDestination(String destination) {
	this.setValue(By.id(_lcapp.getValue("lcFinalDestination")), destination, 5);
}
public void setShipmentPeriod(String period) {
	this.setValue(By.id(_lcapp.getValue("lcShipmentPeriod")), period, 5);
}
public void setLatestShipmentDate(String date) {
	this.setValue(By.id(_lcapp.getValue("lcShipmentDate")), date, 5);
}
public void setDescriptionOfGoodsAndServices(String goodsandservice) {
	this.setValue(By.id(_lcapp.getValue("lcGoodDesc")), goodsandservice, 5);
}
public void setIncoterms(String incoterms) {
	this.setDropdownValueByVal(By.id(_lcapp.getValue("lcIncoterms")), incoterms, 5);
}
public void clickWarStrike() {
	this.clickElement(By.id(_lcapp.getValue("lcWarstrike")), 5);
}
public void clickTheft() {
	this.clickElement(By.id(_lcapp.getValue("lcTheft")), 5);
}
public void clickNonDelivery() {
	this.clickElement(By.id(_lcapp.getValue("lcNonDelivery")), 5);
}
public void clickIncotermsOther() {
	this.clickElement(By.id(_lcapp.getValue("lcIncoOthers")), 5);
}
public void setInsuranceCoverage(String insuranceCoverage) {
	this.setValue(By.id(_lcapp.getValue("lcInsuranceCoverage")), insuranceCoverage, 5);
}
public void setSpecifyIncoterm(String incoterm) {
	this.setValue(By.id(_lcapp.getValue("lcIncoOthers")), incoterm, 5);
}
public void setSpecifyOtherInsuranceClause(String clause) {
	this.setValue(By.id(_lcapp.getValue("lcInsuranceClause")), clause, 5);
}
public void clickInsuranceCoveredByApplicant() {
	this.clickElement(By.xpath(_lcapp.getValue("lcInsuranceCoveredByApplicant")), 5);
}
public void clickInsuranceCoveredByUltimateBuyer() {
	this.clickElement(By.xpath(_lcapp.getValue("lcInsuranceCoveredByUltimateBuyer ")), 5);
}
public void setSpecifyIncoterms(String insuranceCoverage) {
	this.setValue(By.id(_lcapp.getValue("lcInsuranceCoverage")), insuranceCoverage, 5);
}

public String getPlaceOfReceipt() {
	return this.getElementAttributeText(By.id(_lcapp.getValue("lcPlaceOfReceipt")), "value", 10);
}


//Additional Terms
public void clickAdditionalTermsTab() {
	this.clickElement(By.id(_lcapp.getValue("lcAdditionalTermsTab")),5);
}

public void clickSearchBtnOnAdvisingBankBIC() {
	this.clickElement(By.xpath(_lcapp.getValue("lcAdvisingBankSearchBtn")), 5);
}
public void clickSearchBtnOnAdvisingBankName() {
	this.clickElement(By.xpath(_lcapp.getValue("lcAdvisingBankNameSearchBtn")), 5);
}
public void setConfirmationInstructions(String confirmation) {
	this.setDropdownValueByVal(By.xpath(_lcapp.getValue("lcConfirmationInstruction")), confirmation, 5);
}
public void clickSearchBtnOnRequestedPartyBIC() {
	this.clickElement(By.xpath(_lcapp.getValue("lcAdvisingBankSearchBtn")), 5);
}
public void clickSearchBtnOnARequestedPartyName() {
	this.clickElement(By.xpath(_lcapp.getValue("lcAdvisingBankNameSearchBtn")), 5);
}

//Settlement Instructions
public void clickSearchBtnOnPrincipalAccountNo() {
	this.clickElement(By.xpath(_lcapp.getValue("lcPrincipalAccountSearchBtn")), 5);
}
public void clickAddNewPrincipalAccount() {
	this.clickElement(By.xpath(_lcapp.getValue("lcAddNewPrincipalAcountBtn")), 5);
}

public void clickSearchBtnOnSettlementAccountNo() {
	this.clickElement(By.xpath(_lcapp.getValue("lcSettlementAccountSearchBtn")), 5);
}
public void clickAddNewSettlementAccount() {
	this.clickElement(By.xpath(_lcapp.getValue("lcAddNewSettlementAcountBtn")), 5);
}

public void setPrincipalSettlementCurrency(String currency) {
	this.setDropdownValueByVal(By.xpath(_lcapp.getValue("lcPrincipalSettlementCurrency")), currency, 5);
}
public void setChargeSettlementCurrency(String currency) {
	this.setDropdownValueByVal(By.xpath(_lcapp.getValue("lcChargeSettlementCurrency")), currency, 5);
}
public void setCustomerInstruction(String instruction) {
	this.setValue(By.xpath(_lcapp.getValue("lcCustomerInstruction")), instruction, 5);
}
}
