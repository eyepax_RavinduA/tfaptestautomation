package tfap.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import tfap.common.seleniumActions;
import tfap.utills.fileReader;

public class applicationListPage extends seleniumActions {
	WebDriver _driver;
	public fileReader _locator = new fileReader("src/main/resources/locators/listingPageLocators.properties");
	public applicationListPage(WebDriver driver) {
		super(driver);
		_driver = driver;
		// TODO Auto-generated constructor stub
	}


	public void setTFAPIDForSearch(String tfapID) {
		this.setValue(By.id(_locator.getValue("tfapIDSearch")), tfapID, 10);
	}
	
	public void clickActionDropDown() {
		this.clickElement(By.xpath(_locator.getValue("actionDropdown")), 10);
	}
	
	public lcPage viewLCApplication() {
		this.clickElement(By.xpath(_locator.getValue("viewButton")), 5);
		return new lcPage(_driver);
	}
	
	public String getFirstSearchResultID() {
		return this.getElementText(By.xpath(_locator.getValue("firstRecord")),5);
	}
	
	public lcPage clickFirstRecord() {
		this.clickElement(By.xpath(_locator.getValue("firstRecord")), 5);
		return new lcPage(_driver);
	}
}
